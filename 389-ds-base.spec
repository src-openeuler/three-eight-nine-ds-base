%global __provides_exclude ^libjemalloc\\.so.*$
%global _hardened_build 1
%{!?with_tmpfiles_d: %global with_tmpfiles_d %{_sysconfdir}/tmpfiles.d}
ExcludeArch:   i686

Name:          389-ds-base
Summary:       Base 389 Directory Server
Version:       3.1.1
Release:       5
License:       GPLv3+
URL:           https://www.port389.org
Source0:       https://releases.pagure.org/389-ds-base/389-ds-base-%{version}.tar.bz2
Source1:       389-ds-base-git.sh
Source2:       389-ds-base-devel.README

# Refer: https://github.com/389ds/389-ds-base/pull/5374
Patch0:        fix-dsidm-posixgroup-get_dn-fails-with-search_ext.patch
Patch1:        remove-where-cockpit_present-is-called.patch
Patch2:        fix-dsidm-role-subtree-status-fails-with-TypeError.patch
# https://github.com/sfackler/rust-openssl/commit/f014afb230de4d77bc79dea60e7e58c2f47b60f2
Patch3:        CVE-2025-24898.patch

BuildRequires: nspr-devel nss-devel >= 3.34 perl-generators openldap-devel libdb-devel cyrus-sasl-devel icu
BuildRequires: libicu-devel pcre-devel cracklib-devel gcc-c++ net-snmp-devel lm_sensors-devel bzip2-devel
BuildRequires: zlib-devel openssl-devel pam-devel systemd-units systemd-devel pkgconfig pkgconfig(systemd)
BuildRequires: pkgconfig(krb5) autoconf automake libtool doxygen libcmocka-devel libevent-devel chrpath
BuildRequires: python%{python3_pkgversion} python%{python3_pkgversion}-devel python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-ldap python%{python3_pkgversion}-six python%{python3_pkgversion}-pyasn1
BuildRequires: python%{python3_pkgversion}-pyasn1-modules python%{python3_pkgversion}-dateutil
BuildRequires: python%{python3_pkgversion}-argcomplete python%{python3_pkgversion}-argparse-manpage
BuildRequires: python%{python3_pkgversion}-libselinux python%{python3_pkgversion}-policycoreutils
BuildRequires: python%{python3_pkgversion}-packaging rsync npm nodejs libtalloc-devel libtevent-devel
BuildRequires: lmdb-devel json-c-devel cargo python3-cryptography
Requires:      389-ds-base-libs = %{version}-%{release}
Requires:      python%{python3_pkgversion}-lib389 = %{version}-%{release}
Requires:      policycoreutils-python-utils /usr/sbin/semanage libsemanage-python%{python3_pkgversion}
Requires:      selinux-policy >= 3.14.1-29 openldap-clients openssl-perl python%{python3_pkgversion}-ldap
Requires:      nss-tools nss >= 3.34 krb5-libs libevent cyrus-sasl-gssapi cyrus-sasl-md5 cyrus-sasl-plain
Requires:      libdb-utils
Requires:      perl-Errno >= 1.23-360 perl-DB_File perl-Archive-Tar cracklib-dicts
%{?systemd_requires}

Provides:      389-ds-base-libs = %{version}-%{release} svrcore = 4.1.4 ldif2ldbm >= 0
Obsoletes:     389-ds-base-libs < %{version}-%{release}
Obsoletes:     svrcore <= 4.1.3 389-ds-base <= 1.3.5.4 389-ds-base <= 1.4.0.9
Conflicts:     svrcore selinux-policy-base < 3.9.8 freeipa-server < 4.0.3

%description
389-ds-base is an LDAPv3 compliant server which includes
the LDAP server and command line utilities for server administration.

%package       devel
Summary:       Development libraries for 389 Directory Server
Requires:      389-ds-base-libs = %{version}-%{release} pkgconfig nspr-devel nss-devel >= 3.34
Requires:      openldap-devel libtalloc libevent libtevent systemd-libs
Provides:      svrcore-devel = 4.1.4
Conflicts:     svrcore-devel
Obsoletes:     svrcore-devel <= 4.1.3

%description   devel
Development Libraries and headers for the 389 Directory Server.

%package       snmp
Summary:       SNMP Agent for 389 Directory Server
Requires:      389-ds-base = %{version}-%{release}
Obsoletes:     389-ds-base <= 1.4.0.0

%description   snmp
SNMP Agent for the 389 Directory Server.

%package       -n python%{python3_pkgversion}-lib389
Summary:       Library for accessing, testing, and configuring 389 Directory Server
BuildArch:     noarch
Requires:      krb5-workstation krb5-server openssl iproute python%{python3_pkgversion}
Requires:      python%{python3_pkgversion}-ldap python%{python3_pkgversion}-six
Requires:      python%{python3_pkgversion}-pyasn1 python%{python3_pkgversion}-pyasn1-modules
Requires:      python%{python3_pkgversion}-dateutil python%{python3_pkgversion}-argcomplete
Requires:      python%{python3_pkgversion}-libselinux
%{?python_provide:%python_provide python%{python3_pkgversion}-lib389}

%description   -n python%{python3_pkgversion}-lib389
Tools and libraries for accessing, testing, and configuring the 389 Directory Server.

%package       -n cockpit-389-ds
Summary:       Cockpit UI Plugin for configuring and administering 389 Directory Server
BuildArch:     noarch
Requires:      cockpit python%{python3_pkgversion} python%{python3_pkgversion}-lib389

%description   -n cockpit-389-ds
A cockpit UI Plugin for configuring and administering the 389 Directory Server

%package       help
Summary:       Documentation for 389 Directory Server
Requires:      389-ds-base = %{version}-%{release}

%description   help
Documentation for 389 Directory Server.

%prep
%autosetup -n 389-ds-base-%{version} -p1

# fix typo
sed -i 's/sucessfully/successfully/g' src/lib389/lib389/cli_conf/backend.py

cp %{SOURCE2} README.devel

%build

OPENLDAP_FLAG="--with-openldap"
%{?with_tmpfiles_d: TMPFILES_FLAG="--with-tmpfiles-d=%{with_tmpfiles_d}"}
NSSARGS="--with-nss-lib=%{_libdir} --with-nss-inc=%{_includedir}/nss3"

RUST_FLAGS="--enable-rust --enable-rust-offline"

LEGACY_FLAGS="--enable-legacy --enable-perl"

%define _strict_symbol_defs_build 1
autoreconf -fiv
%configure --enable-autobind --with-selinux $OPENLDAP_FLAG $TMPFILES_FLAG --with-systemd \
           --with-systemdsystemunitdir=%{_unitdir} \
           --with-systemdsystemconfdir=%{_sysconfdir}/systemd/system \
           --with-systemdgroupname=dirsrv.target --libexecdir=%{_libexecdir}/dirsrv \
           $NSSARGS $ASAN_FLAGS $RUST_FLAGS $PERL_FLAGS $CLANG_FLAGS $LEGACY_FLAGS --enable-cmocka --enable-perl --with-libldap-r=no

make src/lib389/setup.py
cd ./src/lib389
%py3_build
cd -
for f in "dsconf.8" "dsctl.8" "dsidm.8" "dscreate.8"; do
  sed -i  "1s/\"1\"/\"8\"/" %{_builddir}/389-ds-base-%{version}/src/lib389/man/$f
done
export XCFLAGS=$RPM_OPT_FLAGS
%make_build

%install
install -d %{buildroot}%{_datadir}/gdb/auto-load%{_sbindir}
install -d %{buildroot}%{_datadir}/cockpit
%make_install

find %{buildroot}%{_datadir}/cockpit/389-console -type d | sed -e "s@%{buildroot}@@" | sed -e 's/^/\%dir /' > cockpit.list
find %{buildroot}%{_datadir}/cockpit/389-console -type f | sed -e "s@%{buildroot}@@" >> cockpit.list
cp -r %{_builddir}/389-ds-base-%{version}/man/man3 $RPM_BUILD_ROOT/%{_mandir}/man3

cd src/lib389
%py3_install
cd -

for t in "log" "lib" "lock"; do
  install -d $RPM_BUILD_ROOT/var/$t/dirsrv
done

install -d $RPM_BUILD_ROOT%{_sysconfdir}/systemd/system/dirsrv.target.wants

%delete_la

cd  $RPM_BUILD_ROOT/usr
file `find -type f`| grep -w ELF | awk -F":" '{print $1}' | for i in `xargs`
do
  chrpath -d $i
done
cd -
mkdir -p  $RPM_BUILD_ROOT/etc/ld.so.conf.d
echo "%{_bindir}/%{name}" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf
echo "%{_libdir}/%{name}" >> $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf
echo "%{_libdir}/dirsrv/plugins" >> $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf
echo "%{_libdir}/dirsrv" >> $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%check
if ! make DESTDIR="$RPM_BUILD_ROOT" check; then
  cat ./test-suite.log && false;
fi

%post
/sbin/ldconfig
if [ -n "$DEBUGPOSTTRANS" ] ; then
    output=$DEBUGPOSTTRANS
    output2=${DEBUGPOSTTRANS}.upgrade
else
    output=/dev/null
    output2=/dev/null
fi

/bin/systemctl daemon-reload >$output 2>&1 || :

USERNAME="dirsrv"
ALLOCATED_UID=389
GROUPNAME="dirsrv"
ALLOCATED_GID=389
HOMEDIR="/usr/share/dirsrv"

getent group $GROUPNAME >/dev/null || /usr/sbin/groupadd -f -g $ALLOCATED_GID -r $GROUPNAME
if ! getent passwd $USERNAME >/dev/null ; then
    if ! getent passwd $ALLOCATED_UID >/dev/null ; then
      /usr/sbin/useradd -r -u $ALLOCATED_UID -g $GROUPNAME -d $HOMEDIR -s /sbin/nologin -c "user for 389-ds-base" $USERNAME
    else
      /usr/sbin/useradd -r -g $GROUPNAME -d $HOMEDIR -s /sbin/nologin -c "user for 389-ds-base" $USERNAME
    fi
fi

sysctl --system &> $output; true

instances=""
ninst=0

echo looking for instances in %{_sysconfdir}/dirsrv > $output 2>&1 || :
instbase="%{_sysconfdir}/dirsrv"
for dir in $instbase/slapd-* ; do
    echo dir = $dir >> $output 2>&1 || :
    if [ ! -d "$dir" ] ; then continue ; fi
    case "$dir" in *.removed) continue ;; esac
    basename=`basename $dir`
    inst="dirsrv@`echo $basename | sed -e 's/slapd-//g'`"
    echo found instance $inst - getting status  >> $output 2>&1 || :
    if /bin/systemctl -q is-active $inst ; then
       echo instance $inst is running >> $output 2>&1 || :
       instances="$instances $inst"
    else
       echo instance $inst is not running >> $output 2>&1 || :
    fi
    ninst=`expr $ninst + 1`
done
if [ $ninst -eq 0 ] ; then
    echo no instances to upgrade >> $output 2>&1 || :
    exit 0
fi

echo shutting down all instances . . . >> $output 2>&1 || :
for inst in $instances ; do
    echo stopping instance $inst >> $output 2>&1 || :
    /bin/systemctl stop $inst >> $output 2>&1 || :
done
echo remove pid files . . . >> $output 2>&1 || :
/bin/rm -f /var/run/dirsrv*.pid /var/run/dirsrv*.startpid

echo upgrading instances . . . >> $output 2>&1 || :
DEBUGPOSTSETUPOPT=`/usr/bin/echo $DEBUGPOSTSETUP | /usr/bin/sed -e "s/[^d]//g"`
if [ -n "$DEBUGPOSTSETUPOPT" ] ; then
    %{_sbindir}/setup-ds.pl -$DEBUGPOSTSETUPOPT -u -s General.UpdateMode=offline >> $output 2>&1 || :
else
    %{_sbindir}/setup-ds.pl -u -s General.UpdateMode=offline >> $output 2>&1 || :
fi

for inst in $instances ; do
    echo restarting instance $inst >> $output 2>&1 || :
    /bin/systemctl start $inst >> $output 2>&1 || :
done

%preun
if [ $1 -eq 0 ]; then
  rm -rf %{_sysconfdir}/systemd/system/dirsrv.target.wants/* > /dev/null 2>&1 || :
fi

%postun
/sbin/ldconfig
if [ $1 = 0 ]; then
  rm -rf /var/run/dirsrv
fi

%post          snmp
mkdir -p /run/dirsrv
%systemd_post dirsrv-snmp.service

%preun         snmp
%systemd_preun dirsrv-snmp.service dirsrv.target

%postun        snmp
%systemd_postun_with_restart dirsrv-snmp.service

exit 0

%files
%doc LICENSE LICENSE.GPLv3+ LICENSE.openssl
%{_libdir}/libsvrcore.so.*
%{_libdir}/dirsrv/{libslapd.so.*,libns-dshttpd.so.*,libsds.so.*,libldaputil.so.*,librewriters.so*}
%dir %{_sysconfdir}/dirsrv
%dir %{_sysconfdir}/dirsrv/schema
%config(noreplace)%{_sysconfdir}/dirsrv/schema/*.ldif
%dir %{_sysconfdir}/dirsrv/config
%dir %{_sysconfdir}/systemd/system/dirsrv.target.wants
%config(noreplace)%{_sysconfdir}/dirsrv/config/{slapd-collations.conf,certmap.conf,template-initconfig}
%{_datadir}/dirsrv
%{_datadir}/gdb/auto-load/*
%{_unitdir}
%{_bindir}/{dbscan,ds-replcheck,ds-logpipe.py,ldclt,logconv.pl,pwdhash}
%{_sbindir}/ns-slapd
%{_mandir}/man8/ns-slapd.8.gz
%{_sbindir}/openldap_to_ds
%{_mandir}/man8/openldap_to_ds.8.gz
%{_libexecdir}/dirsrv/ds_systemd_ask_password_acl
%{_libexecdir}/dirsrv/ds_selinux_restorecon.sh
%{_libdir}/dirsrv/python
%dir %{_libdir}/dirsrv/plugins
%{_libdir}/dirsrv/plugins/*.so
%{_prefix}/lib/sysctl.d/*
%dir %{_localstatedir}/lib/dirsrv
%dir %{_localstatedir}/log/dirsrv
%ghost %dir %{_localstatedir}/lock/dirsrv
%exclude %{_sbindir}/ldap-agent*
%exclude %{_unitdir}/dirsrv-snmp.service
%config(noreplace) /etc/ld.so.conf.d/*

%files         devel
%doc LICENSE LICENSE.GPLv3+ LICENSE.openssl
%{_mandir}/man3/*
%{_includedir}/svrcore.h
%{_includedir}/dirsrv
%{_libdir}/libsvrcore.so
%{_libdir}/dirsrv/{libslapd.so,libns-dshttpd.so,libsds.so,libldaputil.so}
%{_libdir}/pkgconfig/{svrcore.pc,dirsrv.pc,libsds.pc}

%files         snmp
%doc LICENSE LICENSE.GPLv3+ LICENSE.openssl
%config(noreplace)%{_sysconfdir}/dirsrv/config/ldap-agent.conf
%{_sbindir}/ldap-agent*
%{_unitdir}/dirsrv-snmp.service

%files         -n python%{python3_pkgversion}-lib389
%doc LICENSE LICENSE.GPLv3+
%{python3_sitelib}/lib389*
%{_sbindir}/{dsconf,dscreate,dsctl,dsidm}
%{_libexecdir}/dirsrv/dscontainer

%files         -n cockpit-389-ds -f cockpit.list
%{_datarootdir}/metainfo/389-console/org.port389.cockpit_console.metainfo.xml

%files         help
%doc README.md README.devel
%{_mandir}/*/*

%changelog
* Thu Feb 06 2025 yaoxin <1024769339@qq.com> - 3.1.1-5
- Fix CVE-2025-24898

* Fri Nov 29 2024 wangkai <13474090681@163.com> - 3.1.1-4
- Fix typo sucessfully

* Tue Nov 26 2024 wangkai <13474090681@163.com> - 3.1.1-3
- Fix dsidm role subtree-status fails with TypeError

* Mon Nov 25 2024 xu_ping <707078654@qq.com> - 3.1.1-2
- fix name cockpit_present is not defined.

* Thu Aug 01 2024 yaoxin <yao_xin001@hoperun.com> - 3.1.1-1
- Update to 3.1.1
  * Security fix for CVE-2024-6237,CVE-2024-5953,CVE-2024-3657,CVE-2024-2199
  * Issue 6172 - RFE: improve the performance of evaluation of filter component 
    when tested against a large valueset (like group members) #6173
  * Issue 6181 - RFE - Allow system to manage uid/gid at startup
  * Issue 6238 - RFE - add option to write audit log in JSON format
  * Issue 6241 - Add support for CRYPT-YESCRYPT #6242
  * Issue 5772 - ONE LEVEL search fails to return sub-suffixes #6219
  * Issue 6123 - Allow DNA plugin to reuse global config for bind method and connection protocol #6124
  * Issue 6155 - ldap-agent fails to start because of permission error #6179
  * Issue 6170 - audit log buffering doesn’t handle large updates
  * Issue 6175 - Referential integrity plugin - in referint_thread_func does not handle null from ldap_utf8strtok #6168
  * Issue 6183 - Slow ldif2db import on a newly created BDB backend #6208
  * Issue 6199 - unprotected search query during certificate based authentication #6205
  * Issue 6224 - d2entry - Could not open id2entry err 0 - at startup when having sub-suffixes #6225
  * Issue 6229 - After an initial failure, subsequent online backups fail #6230
  * Issue 6254 - Enabling replication for a sub suffix crashes browser #6255
  * Issue 6256 - nsslapd-numlisteners limit is not enforced
  * Issue 6265 - lmdb - missing entries in range searches #6266
  * Please see log - https://www.port389.org/docs/389ds/releases/release-3-1-1

* Wed Jun 05 2024 wangkai <13474090681@163.com> - 2.3.2-7
- Fix CVE-2024-2199 and CVE-2024-3657

* Wed Mar 27 2024 liyanan <liyanan61@h-partners.com> - 2.3.2-6
- Delete the non-existing command readnsstate

* Mon Feb 05 2024 wangkai <13474090681@163.com> - 2.3.2-5
- Fix CVE-2024-1062

* Fri Oct 27 2023 wangkai <13474090681@163.com> - 2.3.2-4
- Fix dsidm posixgroup get_dn fails with search_ext()

* Thu Aug 03 2023 wulei <wu_lei@hoperun.com> - 2.3.2-3
- Fix using `.borrow()` on a double reference

* Tue Jul 18 2023 xu_ping <707078654@qq.com> - 2.3.2-2
- Replace LegacyVersion with DSVersion to fix build error.

* Fri Apr 21 2023 wulei <wu_lei@hoperun.com> - 2.3.2-1
- Upgrade package to version 2.3.2

* Fri Aug 05 2022 wangkai <wangkai385@h-partners.com> - 1.4.3.20-1
- Update to 1.4.3.20 for fix CVE-2020-35518

* Tue Apr 19 2022 yaoxin <yaoxin30@h-partners.com> - 1.4.0.31-6
- Resolve compilation failures

* Wed Sep 22 2021 liwu<liwu13@huawei.com> - 1.4.0.31-5
- fix CVE-2021-3652 CVE-2021-3514

* Wed Sep 08 2021 chenchen <chen_aka_jan@163.com> - 1.4.0.31-4
- del rpath from some binaries and bin

* Mon Aug 2 2021 Haiwei Li <lihaiwei8@huawei.com> - 1.4.0.31-3
- Fix complication failed due to gcc upgrade

* Wed Apr 29 2020 lizhenhua <lizhenhua21@huawei.com> - 1.4.0.31-2
- Package init
